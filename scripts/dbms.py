import peewee as pw

db = pw.SqliteDatabase('db/data.db')


class BaseModel(pw.Model):
    class Meta:
        database = db


class Users(BaseModel):
    id = pw.PrimaryKeyField(null=False)
    username = pw.CharField(max_length=255)
    email = pw.TextField(unique=True)
    date_requested = pw.IntegerField
    total_submissions = pw.IntegerField


class SubmissionData(BaseModel):
    user = pw.ForeignKeyField(Users, related_name="details")
    label = pw.TextField(null=True)
    statement = pw.TextField
    solution = pw.TextField
    pdf_link = pw.TextField
    test_case_link = pw.TextField
    challenge_link = pw.TextField


class ProcessingLog(BaseModel):
    user = pw.ForeignKeyField(Users, related_name="details")
    submission_url = pw.TextField
    scraped = pw.BooleanField(default=False)

