from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from lxml import html
import time
import pyperclip
import json

import logging


def process_submissisons(page_source):
    # extract all links here
    # process all page numbers
    # return list of links
    data = []
    page = html.fromstring(page_source)
    # logging.debug("page source is {}".format(page_source))
    last_link = page.xpath('//a[@data-attr1="Last"]')
    if last_link:

        logging.info("last link is not empty it is - {}".format(last_link))

        last_url = last_link[0].attrib['href']
        page_max = int(last_url.split(sep="/")[-1])
        prefix = last_url[0:len(last_url) - len(str(page_max))]

        logging.info("page_max is - {} \nprefix is - {}".format(page_max, prefix))

        for i in range(1, page_max + 1, -1):
            link = prefix + str(i)
            print("processing {}".format(link))
            logging.info("opening {}".format(link))
            driver.get(link)
            WebDriverWait(driver, 20).until(EC.text_to_be_present_in_element((By.XPATH, "//p[contains(@class, 'small accepted')]"), "Accepted"))
            # logging.debug("page source is {}".format(page_source))
            code = html.fromstring(driver.page_source)
            items = code.xpath('//div[contains(@class, "table-body submissions-list-wrapper")]/div[contains(@class, "chronological-submissions-list")]')
            for item in items:
                data.append(extract_submissions(item))
    else:
        logging.debug("page source is {}".format(page_source))
        code = html.fromstring(driver.page_source)
        items = code.xpath('//div[contains(@class, "table-body submissions-list-wrapper")]/div[contains(@class, "chronological-submissions-list")]')
        for item in items:
            data.append(extract_submissions(item))

    return data


def login(username, password):

    username_input = driver.find_element_by_css_selector('input[name=username]')
    password_input = driver.find_element_by_css_selector('input[name=password]')

    # username.clear()
    username_input.send_keys(username)
    # password.clear()
    password_input.send_keys(password)

    driver.find_element_by_css_selector('button[data-analytics=LoginPassword]').click()



def extract_submissions(item):
    data = {
        'title': " ".join(item.xpath('div[contains(@class, "submissions_item")]/div[@class="span4 submissions-title"]/p/a/text()')[0].split()),
        'language': " ".join(item.xpath('div[contains(@class, "submissions_item")]/div[@class="span2 submissions-language"]/p/text()')[0].split()),
        'result': " ".join(item.xpath('div[contains(@class, "submissions_item")]/div[contains(@class,"span3")]/p/text()')[0].split()),
        'score': " ".join(item.xpath('div[contains(@class, "submissions_item")]/div[@class="span1"]/p/text()')[0].split()),
        'url': "https://www.hackerrank.com/" + item.xpath('div[contains(@class, "submissions_item")]/div[contains(@class,"btn-wrap")]/p/a/@href')[0],
    }
    return data


def extract_data(url):
    # open link
    driver.get(url.replace('/submissions/code/', '/copy-from/'))
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Submit Code')]")))

    # extract problem label
    label = driver.find_elements_by_xpath("//h1[contains(@class, 'page-label')]")[0].text

    # extract problem statement
    statement = driver.find_elements_by_xpath("//div[@class='challenge-body-html']")[0].text

    # modify problem statement
    # extract problem statement download link
    pdf_link = driver.find_elements_by_xpath("//a[@id='pdf-link']")[0].get_attribute('href')

    # extract test cases
    test_case_link = driver.find_elements_by_xpath("//a[@id='test-cases-link']")[0].get_attribute('href')

    # extract submitted code
    driver.find_element_by_css_selector("div[class=view-line]").click()
    select_all_action = ActionChains(driver).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL)
    copy_action = ActionChains(driver).key_down(Keys.CONTROL).send_keys('c').key_up(Keys.CONTROL)
    select_all_action.perform()
    time.sleep(1)
    copy_action.perform()
    time.sleep(1)
    code = pyperclip.paste()
    # save data somewhere maybe in json or db
    # return link to db or json
    extracted_data = {"label": label, "statement": statement, "pdf_link": pdf_link, "test_case_link": test_case_link, "code": code}
    return extracted_data


if __name__ == "__main__":

    print("Opening Web Driver...")
    options = Options()
    options.add_argument("--headless")
    logging.basicConfig(filename="execution.log", level=10)  # logger settings

    # driver = webdriver.Firefox(firefox_options=options)
    driver = webdriver.Firefox()  # defining firefox web driver
    wait = WebDriverWait(driver, 20)
    print("Opening LOGIN page..")
    driver.get("http://www.hackerrank.com/login")
    print("Logging you in with specified userid and password.")
    # take a username
    # take a password
    login("irotect@gmail.com", "mypdv210w")

    # login and wait for the page to load properly
    print("Opening Submissions page")

    # www.hackerrank.com/dashboard goes here
    driver.get('https://www.hackerrank.com/submissions')
    time.sleep(10)
    # pdb.set_trace()
    print("Extracting submission links")

    # submissions page goes here
    WebDriverWait(driver, 20).until(EC.text_to_be_present_in_element((By.XPATH, "//p[contains(@class, 'small accepted')]"), "Accepted"))
    submission_data = process_submissisons(driver.page_source)

    # submissions extracted successfully
    if not submission_data:
        print("No submissions were found in the account.")
        exit()
    else:
        print("{} submissions were found.\nProcessing them one by one.".format(len(submission_data)))
    solution_data = []

    for data in submission_data:
        print("Processing: {}\n".format(data['url']))
        solution_data.append(extract_data(data['url']))

    print("All submissions extracted successfully.\nWriting submission data to file.")

    with open("extraction_data.json", "w") as out_file:
        out_file.write(json.dumps(solution_data))
    print("Data written to 'extraction_data.json'\nQuiting....")
    driver.close()
    driver.quit()  # remember to quit

# present your work
# database framework is ready check it in dbms.py
# present in pdf,jpg,html
# optimise the outputs which are displayed in the compiler while runtime
# make a interface in html for likhopao site
